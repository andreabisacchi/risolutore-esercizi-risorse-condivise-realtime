package model;

public enum Azione {
WAIT("-"), RUN("n"), RISORSA(""), BLOCKED("/"), ENDED(" "), NOTSTARTED(" ");

	
	private final String stampa;
	
	private Azione(String stampa) {
		this.stampa = stampa;
	}
	@Override
	public String toString() {
		return this.stampa;
	}
}
