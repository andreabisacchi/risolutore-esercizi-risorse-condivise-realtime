package model;
import java.util.HashMap;
import java.util.Map;

public class Risorse {
	
	private static Map<Integer, Risorsa> risorse = new HashMap<>();
	
	public static Risorsa getRisorsa(int i) {
		Risorsa risorsa = risorse.get(i);
		if (risorsa == null) {
			risorsa = new Risorsa();
			risorsa.i = i;
			risorse.put(i, risorsa); // se non c'� la creo
		}
		return risorsa;
	}
	
	public static boolean isFree(int i, Processo processo) {
		Risorsa risorsa = risorse.get(i);
		if (risorsa == null) {
			risorsa = new Risorsa();
			risorsa.i = i;
			risorse.put(i, risorsa); // se non c'� la creo
		}
		if (risorsa.bloccante == processo) // se la risorsa � bloccata dal processo corrente -> � libera
			return true;
		else // altrimenti controllo che sia effettivamente libera
			return risorsa.libera;
	}
	
	public static void acquire(int i, Processo processo) throws InterruptedException {
		Risorsa risorsa = risorse.get(i);
		if (risorsa == null) {
			risorsa = new Risorsa();
			risorsa.i = i;
			risorse.put(i, risorsa); // se non c'� la creo
		}
		risorsa.bloccante = processo;
		risorsa.libera = false;
	}
	
	public static void release(int i) {
		Risorsa risorsa = risorse.get(i);
		if (risorsa == null) {
			risorsa = new Risorsa();
			risorse.put(i, risorsa); // se non c'� la creo
		}
		risorsa.libera = true;
		risorsa.bloccante = null;
	}
	
	public static class Risorsa {
		public boolean libera = true;
		public Processo bloccante = null;
		public int i;
	}
	
}
