package model;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Semaphore;

import control.Esecuzione;
import control.logica.Logica;

public class Processo implements Runnable {

	public final int fi;
	public final Queue<List<Integer>> azioni;
	public final int i;
	
	public final Stack<Integer> priorita = new Stack<>();
	public final List<Integer> risorseCheMiHannoFattoPromuovere = new LinkedList<>();
	public final Set<Processo> processiCuiCedutoPriorita = new HashSet<>();

	public Processo(int i, int fi, Queue<Integer[]> azioni) {
		this.i = i;
		this.fi = fi;

		this.priorita.add(i);
		
		this.azioni = new LinkedList<>();
		for (Integer[] azione : azioni) {
			List<Integer> set = new LinkedList<>();
			for (Integer risorsa : azione) {
				set.add(risorsa);
			}
			this.azioni.add(set);
		}
	}
	
	public int getCurrentPriority() {
		return this.priorita.peek();
	}
	
	public void setCurrentPriority(int currentPriority) {
		if (this.priorita.get(0) != currentPriority)
			this.priorita.push(currentPriority);
	}
	
	public void popCurrentPriority() {
		if (priorita.size() != 1) {
			this.priorita.pop();
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + i;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Processo other = (Processo) obj;
		if (i != other.i)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "P"+i;
	}

	private Esecuzione esecuzione;
	private final Semaphore lock = new Semaphore(0);
	public void setEsecuzione(Esecuzione esecuzione) {
		this.esecuzione = esecuzione;
	}

	public void unlock() {
		this.lock.release();
	}

	@Override
	public void run() {
		try {
			boolean stop = false;
			Logica logica = this.esecuzione.getLogica();
			while (!stop) {
				this.lock.acquire(); // Vengo sbloccato se mi viene richiesto di eseguire

				List<Integer> risorse = this.azioni.peek(); // sbircio su cosa dovrei fare
				if (risorse == null) {
					logica.hoTerminato(this);
					stop = true;
				} else { // ho qualcosa da fare

					boolean risorseLibere = this.risorseLibere(risorse);

					if (risorseLibere) { // le risorse sono libere!
						this.acquisisciRisorse(risorse);
						this.eseguo(risorse);
						logica.hoEseguito(this, risorse);
					} else { // le risorse non sono disponibili
						logica.nonPossoEseguire(this, risorse);
					}

				}
			}
		} catch (Throwable e) {}
	}

	private void eseguo(List<Integer> risorse) {
		List<Integer> risorseCopy = new LinkedList<>(risorse);
		this.azioni.poll(); // Rimuovo il primo elemento, ho appena eseguito
		List<Integer> prossimaAzione = this.azioni.peek();
		if (prossimaAzione != null) {
			risorseCopy.removeAll(prossimaAzione); // Rimuovo tutto quello che far� la prossima esecuzione
		}
		this.liberaRisorse(risorseCopy); // Libero le risorse
	}

	private void liberaRisorse(List<Integer> risorse) {
		for (Integer risorsa : risorse) {
			if (risorsa != 0) {
				Risorse.release(risorsa);
			}
		}
	}

	private boolean risorseLibere(List<Integer> risorse) {
		boolean result = true;
		for (Integer risorsa : risorse) {
			if (risorsa != 0) {
				result = result && Risorse.isFree(risorsa, this);
			}
		}
		return result;
	}

	private void acquisisciRisorse(List<Integer> risorse) throws InterruptedException {
		for (Integer risorsa : risorse) {
			if (risorsa != 0) {
				Risorse.acquire(risorsa, this);
			}
		}
	}

}
