package comparators;

import java.util.Comparator;

import model.Processo;

public class PriorityComparator implements Comparator<Processo> {

	@Override
	public int compare(Processo p1, Processo p2) {
		return p1.i - p2.i;
	}
	
}
