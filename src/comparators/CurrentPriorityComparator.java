package comparators;

import java.util.Comparator;

import model.Processo;

public class CurrentPriorityComparator implements Comparator<Processo> {

	@Override
	public int compare(Processo p1, Processo p2) {
		if (p1.getCurrentPriority() == p2.getCurrentPriority()) {
			if (p1.processiCuiCedutoPriorita.contains(p2)) {
				return 1;
			} else {
				return -1;
			}
		} else {
			return p1.getCurrentPriority() - p2.getCurrentPriority();
		}
	}

}
