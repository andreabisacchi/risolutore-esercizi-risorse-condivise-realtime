package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

import control.Esecuzione;
import control.logica.Logica;
import control.logica.LogicaNOP;
import control.logica.LogicaPIP;
import model.Processo;

public class Main {

	private static final String FILENAME = "input.txt";

	public static void main(String[] args) throws Exception {

		if (args.length < 1) {
			System.err.println("Il primo parametro pu� essere --nop oppure --pip");
			System.exit(1);
		}

		List<Processo> processi = new LinkedList<>();

		parserDaFile(processi);

		Logica logica = null;
		if (args[0].equals("--nop")) {
			logica = new LogicaNOP();
		} else if (args[0].equals("--pip")) {
			logica = new LogicaPIP();
		} else {
			System.err.println("Il primo parametro pu� essere --nop oppure --pip");
			System.exit(2);
		}

		Esecuzione esecuzione = new Esecuzione(processi, logica);

		esecuzione.start();

		System.out.println(esecuzione.toString());
	}

	private static void parserDaFile(List<Processo> processi) throws Exception {
		BufferedReader file = new BufferedReader(new FileReader(FILENAME));
		int nextProcesso = 1;
		String riga;
		while ( (riga = file.readLine()) != null) {
			riga = riga.replace(" ", "");
			if (riga.length() > 0 && riga.charAt(0) != '#') {
				Processo p;
				Queue<Integer[]> pEsecuzione = new LinkedList<>();
				int fi = Integer.parseInt(riga.split("-")[0]);
				riga = riga.split("-")[1];
				StringTokenizer tokenizer = new StringTokenizer(riga, ",");
				while (tokenizer.hasMoreElements()) {
					String[] risorse = tokenizer.nextToken().split("/");
					int size = risorse.length;
					Integer[] arr = new Integer[size];
					for(int i=0; i<size; i++) {
						arr[i] = Integer.parseInt(risorse[i]);
					}
					pEsecuzione.add(arr);
				}
				p = new Processo(nextProcesso, fi, pEsecuzione);
				processi.add(p);
				nextProcesso++;
			}
		}
		file.close();
	}

}
