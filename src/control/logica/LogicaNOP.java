package control.logica;

import java.util.List;
import java.util.Map;

import comparators.PriorityComparator;
import model.Processo;

public class LogicaNOP extends Logica {

	private static final PriorityComparator comparatore = new PriorityComparator();
	
	@Override
	protected void sortProcessi() {
		this.processiInEsecuzione.sort(comparatore);
	}

	@Override
	protected void executeSpecific(int tempo, Map<Processo, List<String>> result) {
		this.processiInEsecuzione.get(0).unlock();
	}

	@Override
	protected void blockedSpecific(Processo processo, List<Integer> risorse) {
		this.processiInEsecuzione.get(0).unlock();
	}

	@Override
	protected void eseguitoSpecific(Processo processo, int risorse) {
	}

	@Override
	protected void hoTerminatoSpecific(Processo processo) {
		this.processiInEsecuzione.get(0).unlock();
	}

}
