package control.logica;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import comparators.CurrentPriorityComparator;
import model.Processo;
import model.Risorse;
import model.Risorse.Risorsa;

public class LogicaPIP extends Logica {
	private static final CurrentPriorityComparator comparatore = new CurrentPriorityComparator();
	
	@Override
	protected void sortProcessi() {
		this.processiInEsecuzione.sort(comparatore);
	}

	@Override
	protected void executeSpecific(int tempo, Map<Processo, List<String>> result) {
		this.processiInEsecuzione.get(0).unlock();
	}

	@Override
	protected void blockedSpecific(Processo processo, List<Integer> risorse) {
		Optional<Risorsa> risorsaCheMiBlocca = risorse.stream().filter(r -> r != 0).map(i -> Risorse.getRisorsa(i)).filter(r -> r.bloccante != processo).findFirst();
		
		risorsaCheMiBlocca.get().bloccante.setCurrentPriority(processo.getCurrentPriority());
		risorsaCheMiBlocca.get().bloccante.risorseCheMiHannoFattoPromuovere.add(risorsaCheMiBlocca.get().i);
		processo.processiCuiCedutoPriorita.add(risorsaCheMiBlocca.get().bloccante);
		
		this.sortProcessi();
		this.processiInEsecuzione.get(0).unlock();
	}
	

	@Override
	protected void eseguitoSpecific(Processo processo, int risorsa) {
		if (processo.risorseCheMiHannoFattoPromuovere.contains(risorsa)) {
			int index = processo.risorseCheMiHannoFattoPromuovere.indexOf(risorsa);
			processo.risorseCheMiHannoFattoPromuovere.remove(index);
			processo.popCurrentPriority();
		}
	}

	@Override
	protected void hoTerminatoSpecific(Processo processo) {
		this.sortProcessi();
		this.processiInEsecuzione.get(0).unlock();
	}

}
