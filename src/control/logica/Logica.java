package control.logica;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import control.Esecuzione;
import model.Processo;

public abstract class Logica {

	private List<Processo> processi;
	protected Esecuzione esecuzione;
	
	public void setProcessi(List<Processo> processi) {
		this.processi = processi;
	}
	
	public void setEsecuzione(Esecuzione esecuzione) {
		this.esecuzione = esecuzione;
	}
	
	
	private Semaphore waitForProcessi = null;
	protected List<Processo> processiInEsecuzione = null; 
	public final void execute(int tempo, Map<Processo, List<String>> result) throws InterruptedException {
		this.waitForProcessi = new Semaphore(0); // Attendo che esegua almeno un processo
		
		this.processi.stream().filter(p -> tempo == p.fi+1).forEach(p -> {
						Thread t = new Thread(p, p.toString());
						t.setDaemon(true);
						t.start();
					}); // Per ogni processo che parte ora avvio il suo Thread
		
		this.processiInEsecuzione = new LinkedList<>(); // Preparo la lista del processi in esecuzione
		this.processi.stream().filter(p -> tempo >= p.fi+1).forEach(p -> this.processiInEsecuzione.add(p));
		this.processi.stream().filter(p -> tempo < p.fi+1).forEach(p -> esecuzione.notStartedProcesso(p));
		
		this.processiInEsecuzione.removeAll(this.esecuzione.getProcessiTerminati());
		
		this.sortProcessi();
		this.executeSpecific(tempo, result);
		
		this.waitForProcessi.acquire(); // Attendo il termine di un processo
		this.processiInEsecuzione.forEach(p -> esecuzione.sameAsBeforeProcesso(p)); // Li imposto come uguale a quello che c'era prima
		this.processiInEsecuzione.clear();
	}

	protected abstract void sortProcessi();
	
	protected abstract void executeSpecific(int tempo, Map<Processo, List<String>> result);
	protected abstract void blockedSpecific(Processo processo, List<Integer> risorse);
	protected abstract void hoTerminatoSpecific(Processo processo);
	protected abstract void eseguitoSpecific(Processo processo, int risorse);

	public void hoTerminato(Processo processo) {
		this.esecuzione.terminatoProcesso(processo);
		this.processiInEsecuzione.remove(processo);
		this.hoTerminatoSpecific(processo);
	}

	public void hoEseguito(Processo processo, List<Integer> risorse) {
		Integer risorsa = -1;
		if (risorse.stream().allMatch(r -> r == 0)) {
			this.esecuzione.eseguitoProcesso(processo);
		} else {
			risorsa = risorse.get(risorse.size() - 1);
			this.esecuzione.eseguitoRisorsaProcesso(processo, risorsa);
		}
		this.processiInEsecuzione.remove(processo);
		
		this.eseguitoSpecific(processo, risorsa);
		
		this.waitForProcessi.release();
	}

	public void nonPossoEseguire(Processo processo, List<Integer> risorse) {
		this.esecuzione.blockedProcesso(processo);
		this.processiInEsecuzione.remove(processo);
		this.blockedSpecific(processo, risorse);
	}
	
}
