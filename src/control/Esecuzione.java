package control;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import control.logica.Logica;
import model.Azione;
import model.Processo;

public class Esecuzione {
	
	private final int tempi;
	private final List<Processo> processi;
	
	private final Logica logica;
	
	public Esecuzione(List<Processo> processi, Logica logica) {
		this.processi = new LinkedList<>(processi);
		this.logica = logica;
		
		this.tempi = this.processi.stream().map(p -> p.azioni.size()).mapToInt(Integer::intValue).sum();
	}
	
	public Logica getLogica() {
		return this.logica;
	}
	
	private Map<Processo, List<String>> result = null;
	private List<Processo> processiTerminati = null;
	public void start() throws InterruptedException {
		this.result = new HashMap<>();
		this.processiTerminati = new LinkedList<>();
		
		for (Processo processo : this.processi) {
			result.put(processo, new LinkedList<String>());
			processo.setEsecuzione(this);
		}
		
		this.logica.setProcessi(processi);
		this.logica.setEsecuzione(this);
		
		for (int i = 1; i <= this.tempi; i++) {
			this.logica.execute(i, result);
		}
	}
	
	public List<Processo> getProcessiTerminati() {
		return this.processiTerminati;
	}

	public void aggiungiAzioneAlRisultato(Processo processo, Azione azione) {
		this.result.get(processo).add(azione.toString());
	}
	
	public void eseguitoProcesso(Processo processo) {
		this.aggiungiAzioneAlRisultato(processo, Azione.RUN);
	}
	
	public void eseguitoRisorsaProcesso(Processo processo, int risorsa) {
		this.result.get(processo).add(risorsa + "");
	}

	public void blockedProcesso(Processo processo) {
		this.aggiungiAzioneAlRisultato(processo, Azione.BLOCKED);
	}
	
	public void terminatoProcesso(Processo processo) {
		this.aggiungiAzioneAlRisultato(processo, Azione.ENDED);
		this.processiTerminati.add(processo);
	}
	
	public void notStartedProcesso(Processo processo) {
		this.aggiungiAzioneAlRisultato(processo, Azione.NOTSTARTED);
	}
	
	public void sameAsBeforeProcesso(Processo processo) {
		Azione azione;
		
		List<String> azioni = this.result.get(processo);
		String ultimaAzione = azioni.get(azioni.size() - 1);
		
		if (ultimaAzione.equals(Azione.BLOCKED.toString())) {
			azione = Azione.BLOCKED;
		} else if (ultimaAzione.equals(Azione.ENDED.toString())) {
			azione = Azione.ENDED;
		} else {
			azione = Azione.WAIT;
		}
		
		this.aggiungiAzioneAlRisultato(processo, azione);
	}
	
	@Override
	public String toString() {
		if (result == null)
			return "Not started";
		
		List<String> last = null;
		StringBuilder string = new StringBuilder();
		for (Processo processo : this.result.keySet()) {
			last = this.result.get(processo);
			
			string.append(processo.toString());
			string.append(" ");
			stringElencoEsecuzione(string, last);
			string.append('\n');
		}
		
		string.append("   ");
		int tempi = last.size();
		
		for (int i = 0; i <= tempi; i++) {
			String tmp = i + "    ";
			if (i != 0)
				tmp = tmp.substring(0, tmp.length() - (int)Math.log10(i));
			string.append(tmp);
			
		}
		string.append('\n');
		
		return string.toString();
	}
	
	private void stringElencoEsecuzione(StringBuilder result, List<String> azioni) {
		final int spazi = 2;
		
		for (String azione : azioni) {
			
			if (spazi > 0) {
				result.append('|');
			}
			
			for (int i = 1; i < spazi; i++)
				result.append(' ');
			
			result.append(azione);
			
			for (int i = 0; i < spazi; i++)
				result.append(' ');
		}
	}
	
}
